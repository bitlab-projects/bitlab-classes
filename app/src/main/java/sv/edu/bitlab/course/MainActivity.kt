package sv.edu.bitlab.course

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var inputEmail: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inputEmail = findViewById(R.id.input_email)

        findViewById<Button>(R.id.button_account).setOnClickListener {

            Glide
                .with(this)
                .load("https://upload.wikimedia.org/wikipedia/commons/6/66/Android_robot.png")
                .into(findViewById(R.id.image_view_account))
        }

    }






}
